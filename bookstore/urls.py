from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from django.views.generic.base import RedirectView
from django.conf.urls.static import static
from bookstore import settings

urlpatterns=[

    path('Bookish/', include('Bookish.urls')),
    path('accounts/', include('registration.backends.default.urls')),
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url="Bookish/")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
