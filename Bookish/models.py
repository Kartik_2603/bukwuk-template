from django.db import models

# Create your models here.
class News(models.Model):
    subject = models.CharField(max_length=100)
    description = models.TextField(null=True)
    n_date = models.DateField()
    image=models.ImageField(upload_to = "images\\", null=True)
    link = models.URLField()

class AdventureBook(models.Model):
    bookname = models.CharField(max_length = 100)
    author = models.CharField(max_length = 100)
    image=models.ImageField(upload_to = "images\\", null=True)
    description = models.TextField(null=True)
    def __str__(self):
        return self.bookname
    
class Toprelease(models.Model):
    name = models.CharField(max_length=40)
    author = models.CharField(max_length=40)
    genre = models.CharField(max_length=40)
    description = models.TextField(null=True)
    image=models.ImageField(upload_to = "images\\", null=True)
    def __str__(self):
        return self.name
    

class ThrillerBooks(models.Model):
    bookname = models.CharField(max_length = 100)
    author = models.CharField(max_length = 100)
    image=models.ImageField(upload_to = "images\\", null=True)
    description = models.TextField(null=True)
    def __str__(self):
        return self.bookname