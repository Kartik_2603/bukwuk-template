from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from Bookish import views
from django.views.generic.base import RedirectView
urlpatterns = [ 
    path('home/', views.HomeView.as_view()),
    path('about/', views.AboutView.as_view()),
    path('contact/', views.ContactView.as_view()),
    path('index/', views.IndexView.as_view()),
    path('main/', views.MainView.as_view()),
    path('news/', views.NewsListView.as_view()),    
    path('news/<int:pk>', views.NewsDetailView.as_view()),
    path('', RedirectView.as_view(url="index/")),    
    path('categories/', views.CategoriesView.as_view()),
    path('quiz/', views.QuizView.as_view()),
     
    path('fiction/', views.FictionView.as_view()), 
    
    path('nonfiction/', views.NonfictionView.as_view()),
    path('thriller/', views.ThrillerBooksListView.as_view()),
    path('thriller/<int:pk>', views.ThrillerBooksDetailView.as_view()),
    
    path('adventure/', views.AdventureBookListView.as_view()),
    path('adventure/<int:pk>', views.AdventureBookDetailView.as_view()),
]
