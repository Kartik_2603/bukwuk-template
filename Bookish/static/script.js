window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("navbar").style.top = "14.85%";
  } else {
    document.getElementById("navbar").style.top = "-50px";
  }
}



 const correctAnswers = ['B','A','B','A'];
 const form = document.querySelector('.quiz-form');
 const result = document.querySelector('.result');
 
 form.addEventListener('submit', e =>{
	 e.preventDefault();
	 
	 let score = 0;
	 const userAnswer = [form.q1.value,form.q2.value,form.q3.value,form.q4.value,];
	 userAnswer.forEach((answer,index) =>{
		 if(answer === correctAnswers[index]){
			 score += 25;
		 }
	 });
	 scrollTo(0,0);
	 
	 result.classList.remove('d-none');
	
	 let output = 0;
	 const timer = setInterval(()=>{
		 result.querySelector('span').textContent = `${output}%`;
		 if(output === score){
			 clearInterval(timer);
		 }else{
			 output++;
		 }
	 },25);
 });