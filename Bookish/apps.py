from django.apps import AppConfig


class BookishConfig(AppConfig):
    name = 'Bookish'
