from django.contrib import admin
from Bookish.models import AdventureBook , Toprelease , ThrillerBooks 
from django.contrib.admin.options import ModelAdmin


# Register your models here.
class AdventureBookAdmin(ModelAdmin):
    list_display = ["bookname", "author"]
    search_fields = ["description",  "bookname"]
    list_filter = ["author"]
admin.site.register(AdventureBook, AdventureBookAdmin)

class TopreleaseAdmin(ModelAdmin):
    list_display = ["name", "author"]
    search_fields = ["name",  "description"]
    list_filter = ["genre","author"]
admin.site.register(Toprelease, TopreleaseAdmin)

class ThrillerBooksAdmin(ModelAdmin):
    list_display = ["bookname", "author"]
    search_fields = ["bookname", "description"]
    list_filter = ["author"]
admin.site.register(ThrillerBooks, ThrillerBooksAdmin)
