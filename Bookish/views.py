from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from Bookish.models import News, ThrillerBooks , AdventureBook
from django.views.generic.detail import DetailView
from django.db.models import Q


# Create your views here.
class HomeView(TemplateView):
    template_name = "Bookish/home.html"

class AboutView(TemplateView):
    template_name = "Bookish/about.html"

class ContactView(TemplateView):
    template_name = "Bookish/contact.html"

class IndexView(TemplateView):
    template_name = "Bookish/index.html"
    
class MainView(TemplateView):
    template_name = "Bookish/main.html"
    
class QuizView(TemplateView):
    template_name = "Bookish/quiz.html"
    
    
class NewsListView(ListView):    
    model = News
    def get_queryset(self):
        return News.objects.order_by("-id")[:10]


class NewsDetailView(DetailView):
    model = News

class CategoriesView(TemplateView):
    template_name = "Bookish/categories.html"


class FictionView(TemplateView):
    template_name = "Bookish/fiction.html"
    
class NonfictionView(TemplateView):
    template_name = "Bookish/nonfiction.html"
    
class ThrillerBooksListView(ListView):
    model = ThrillerBooks
    def get_queryset(self):
        si = self.request.GET.get("si")
        if si == None:
            si = ""
        if self.request.user.is_superuser:
            return ThrillerBooks.objects.order_by("-id")
        else:
            return ThrillerBooks.objects.filter(Q(bookname__icontains = si) | Q(author__icontains = si)).order_by("-id")    
        
class ThrillerBooksDetailView(DetailView):
    model = ThrillerBooks       
    
       
class AdventureBookListView(ListView):
    model = AdventureBook
    def get_queryset(self):
        si = self.request.GET.get("si")
        if si == None:
            si = ""
        if self.request.user.is_superuser:
            return AdventureBook.objects.order_by("-id")
        else:
            return AdventureBook.objects.filter(Q(bookname__icontains = si) | Q(author__icontains = si)).order_by("-id")    
        
class AdventureBookDetailView(DetailView):
    model = AdventureBook       
    
             