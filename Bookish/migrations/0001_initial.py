# Generated by Django 2.2.2 on 2019-07-18 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(max_length=100)),
                ('description', models.TextField(null=True)),
                ('n_date', models.DateField()),
                ('image', models.ImageField(null=True, upload_to='images\\')),
                ('link', models.URLField()),
            ],
        ),
    ]
